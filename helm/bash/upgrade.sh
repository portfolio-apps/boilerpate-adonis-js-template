#!/bin/bash
if [ -n $3 ]
then
helm -n adonis-$1 upgrade --set imageRepo=$2 adonis-$1 ../$1
else
helm -n adonis-$1 upgrade --set imageRepo=$2 --set randomSum=$3 adonis-$1 ../$1
fi

echo "Waiting for External-IP..."
sleep 1
echo "Waiting for External-IP..."
sleep 1
echo "Waiting for External-IP..."
sleep 1
echo "Waiting for External-IP..."
kubectl -n adonis-$1 get all
