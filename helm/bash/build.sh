#!/bin/bash

docker build -t "sk8er71091/adonis:$1" .
docker push "sk8er71091/adonis:$1"

echo ">> YOUR REPO: sk8er71091/adonis:$1"