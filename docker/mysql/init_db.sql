-- Create Databases
CREATE DATABASE IF NOT EXISTS `adonis`;
CREATE DATABASE IF NOT EXISTS `adonis_test`;

-- Create root user and grant rights
-- GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';
GRANT ALL ON *.* TO 'adonis'@'%' IDENTIFIED BY 'test1234' WITH GRANT OPTION;
GRANT ALL ON *.* TO 'adonis_test'@'%' IDENTIFIED BY 'test1234' WITH GRANT OPTION;