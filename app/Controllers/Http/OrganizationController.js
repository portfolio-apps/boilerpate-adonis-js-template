'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

// Models
const Organization = use('App/Models/Organization')

// Validator
const { validate } = use('Validator')

/**
 * Resourceful controller for interacting with organizations
 */
class OrganizationController {
  /**
   * Show a list of all organizations.
   * GET organizations
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const orgs = await Organization.all()
    return response.json({
      success: true,
      organizations: orgs.toJSON()
    })
  }

  /**
   * Render a form to be used for creating a new organization.
   * GET organizations/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new organization.
   * POST organizations
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async store({ request, response, session }) {
    // Validate
    // const validation = await validate(request.all(), {
    //   title: 'required|min:3|max:255',
    //   body: 'required|min:3'
    // })
    // // Check if validation failed
    // if(validation.fails()) {
    //   session.withErrors(validation.messages()).flashAll()
    //   return response.redirect('back')
    // }
    // Create new instance
    const organization = new Organization();
    // Assign detials
    // organization.uuid = request.input('uuid')
    organization.name = request.input('name')
    organization.email = request.input('email')
    organization.website = request.input('website')
    // Save instance
    await organization.save()
    // Show user success
    session.flash({ notification: "Org created!" })
    // return to page
    return response.json({
      success: true,
      organization: organization
    })
  }

  /**
   * Display a single organization.
   * GET organizations/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
     // Find post
     const org = await Organization.find(params.id)
     return response.json({
      success: true,
      organization: org.toJSON()
    })
  }

  /**
   * Render a form to update an existing organization.
   * GET organizations/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update organization details.
   * PUT or PATCH organizations/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response, session }) {
    // Validate
    // const validation = await validate(request.all(), {
    //   title: 'required|min:3|max:255',
    //   body: 'required|min:3'
    // })
    // // Check if validation failed
    // if(validation.fails()) {
    //   session.withErrors(validation.messages()).flashAll()
    //   return response.redirect('back')
    // }
    // Create new instance
    const org = await Organization.find(params.id)

    // Assign detials
    org.name = request.input('name')
    org.email = request.input('email')
    org.website = request.input('website')

    // Save instance
    await org.save()
    // Show user success
    session.flash({ notification: "Organization updated!" })
    // return to page
    return response.json({
      success: true,
      organization: org
    })
  }

  /**
   * Delete a organization with id.
   * DELETE organizations/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async destroy({ params, session, response }) {
      // Find instance
    const org = await Organization.find(params.id)
    // Delete instance
    await org.delete()
    // Show user success
    session.flash({ notification: "Organization deleted!" })
    // return to page
    return response.json({
      success: true,
      message: 'Organization has been deleted'
    })
  }
}

module.exports = OrganizationController
