'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.on('/').render('home')

/** ------------------------------------------------------------
 * Auth Routes
 * -------------------------------------------------------------
 */
Route.post('/register', 'AuthController.register')
Route.post('/login', 'AuthController.login')

/** ------------------------------------------------------------
 * Post Routes
 * -------------------------------------------------------------
 */
Route.get('/posts', 'PostController.index')
Route.get('/posts/add', 'PostController.add')
Route.get('/posts/edit/:id', 'PostController.edit')
Route.get('/posts/:id', 'PostController.details')
Route.post('/posts', 'PostController.store')
Route.put('/posts/:id', 'PostController.update')
Route.delete('/posts/:id', 'PostController.destroy')
// API Routes
Route.get('/api/v1/posts', 'PostController.index');
Route.post('/api/v1/posts', 'PostController.create').middleware('auth')
Route.put('/api/v1/posts/:id', 'PostController.update').middleware('auth')
Route.delete('/api/v1/posts/:id', 'PostController.destroy').middleware('auth')

/** ------------------------------------------------------------
 * Organization Routes
 * -------------------------------------------------------------
 */
// API Routes
Route.get('/api/v1/orgs', 'OrganizationController.index');
Route.post('/api/v1/orgs', 'OrganizationController.store').middleware('auth')
Route.get('/api/v1/orgs/:id', 'OrganizationController.show').middleware('auth')
Route.put('/api/v1/orgs/:id', 'OrganizationController.update').middleware('auth')
Route.delete('/api/v1/orgs/:id', 'OrganizationController.destroy').middleware('auth')

/** ------------------------------------------------------------
 * User Routes
 * -------------------------------------------------------------
 */
Route.get('/users', 'UserController.redis')

 /** ------------------------------------------------------------
 * Role Routes
 * -------------------------------------------------------------
 */
// API Routes
Route.get('/api/v1/roles', 'RoleController.index');
Route.post('/api/v1/roles', 'RoleController.store').middleware('auth')
Route.get('/api/v1/roles/:id', 'RoleController.show').middleware('auth')
Route.put('/api/v1/roles/:id', 'RoleController.update').middleware('auth')
Route.delete('/api/v1/roles/:id', 'RoleController.destroy').middleware('auth')

 /** ------------------------------------------------------------
 * Permission Routes
 * -------------------------------------------------------------
 */
// API Routes
Route.get('/api/v1/permissions', 'PermissionController.index');
Route.post('/api/v1/permissions', 'PermissionController.store').middleware('auth')
Route.get('/api/v1/permissions/:id', 'PermissionController.show').middleware('auth')
Route.put('/api/v1/permissions/:id', 'PermissionController.update').middleware('auth')
Route.delete('/api/v1/permissions/:id', 'PermissionController.destroy').middleware('auth')

