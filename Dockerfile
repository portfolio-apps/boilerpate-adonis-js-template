FROM node:14-alpine

WORKDIR /app

COPY ./package*.json ./

RUN npm i -g @adonisjs/cli && npm install

COPY . /app

CMD ["adonis", "serve", "--dev"]